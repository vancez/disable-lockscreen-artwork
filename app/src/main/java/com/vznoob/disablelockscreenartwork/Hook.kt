package com.vznoob.disablelockscreenartwork

import de.robv.android.xposed.IXposedHookLoadPackage
import de.robv.android.xposed.XC_MethodReplacement
import de.robv.android.xposed.XposedBridge
import de.robv.android.xposed.XposedHelpers.findAndHookMethod
import de.robv.android.xposed.callbacks.XC_LoadPackage

class Hook : IXposedHookLoadPackage {
    override fun handleLoadPackage(lpparam: XC_LoadPackage.LoadPackageParam) {
        if (lpparam.packageName != "com.android.systemui") {
            return
        }

        try {
            findAndHookMethod(
                lpparam.classLoader.loadClass("com.android.systemui.statusbar.NotificationMediaManager"),
                "updateMediaMetaData",
                Boolean::class.java, Boolean::class.java,
                XC_MethodReplacement.DO_NOTHING
            )
        } catch (e: ClassNotFoundException) {
            XposedBridge.log(e)
        }
    }
}